import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Reviews } from '../models/reviews.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {
  constructor( private http: HttpClient) { }

  private reviewList : string = 'http://localhost:8081/api/reviews'

  private httpOptions ={
    headers: new HttpHeaders({
      'Contect-Type': 'application/json'
    })
  };

  getReviews() : Observable<Reviews[]> {
    return this.http.get(this.reviewList,
    this.httpOptions)
    .pipe(map(res => <Reviews[]>res));
    }
}

