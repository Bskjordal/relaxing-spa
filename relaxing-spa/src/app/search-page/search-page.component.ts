import { Component } from '@angular/core';
import { SpaService } from '../service/spa-services.service';
import { SpaServices } from '../models/spa-services.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent {
  srvArray: Array<SpaServices> = [];

  constructor(private SpaService: SpaService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.activatedRoute.queryParams.subscribe((params) => {
      let cat = params['CatId'];

      this.SpaService.getService(cat).subscribe((data: any) => {
        this.srvArray = data;
      })
    }); 
  }
}
