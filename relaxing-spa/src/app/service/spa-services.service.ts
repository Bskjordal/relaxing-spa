 import { Injectable } from '@angular/core';
 import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { SpaServices } from '../models/spa-services.model';
 
@Injectable({
  providedIn: 'root'
})
export class SpaService {

   constructor(private httpClient:HttpClient) { }
   private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };


  // getService() : Observable<SpaServices[]>{ 
    getService(CatId: string):Observable<SpaServices[]>{
return this.httpClient.get("http://localhost:8081/api/services/bycategory/"+ CatId,this.httpOptions).pipe(map(res => <SpaServices[]>res));

  }
}
