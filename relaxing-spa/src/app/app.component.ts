import { Component, OnInit } from '@angular/core';
import { Category } from './models/category.model';
import { CategoryService } from './service/category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  catArray: Array<Category> = [];
  constructor(private CategoryService: CategoryService, private router: Router) { }

  ngOnInit(): void {

    this.CategoryService.getCategory().subscribe((data: any) => {
      this.catArray = data;
    })
  }

  getService(selectedValue: string): void {    
    this.router.navigate(['/search-page'],
      {
        queryParams: {
          CatId: selectedValue
        }
      }
    );
  }
  }
