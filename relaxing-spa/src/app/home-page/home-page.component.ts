import { Component, OnInit } from '@angular/core';
import { Category } from '../models/category.model';
import { Reviews } from '../models/reviews.model';
import { CategoryService } from '../service/category.service';
import { ReviewsService } from '../service/reviews.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  catArray: Array<Category> = [];
  reviewArray: Array<Reviews> = [];

  constructor(private CategoryService: CategoryService, private ReviewsService: ReviewsService, private router: Router) { }

  ngOnInit(): void {

    this.CategoryService.getCategory().subscribe((data: any) => {
      this.catArray = data;
    })

    this.ReviewsService.getReviews().subscribe((data: any) => {
      this.reviewArray = data;
    })
  }

  getService(selectedValue: string): void {
    this.router.navigate(['/search-page'],
      {
        queryParams: {
          CatId: selectedValue
        }
      }
    );
  }
}
