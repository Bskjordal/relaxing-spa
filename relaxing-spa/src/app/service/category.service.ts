import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor( private http: HttpClient) { }

  private catList : string = 'http://localhost:8081/api/categories'

  private httpOptions ={
    headers: new HttpHeaders({
      'Contect-Type': 'application/json'
    })
  };

  getCategory() : Observable<Category[]> {
    return this.http.get(this.catList,
    this.httpOptions)
    .pipe(map(res => <Category[]>res));
    }
}