import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { CategoryService } from './service/category.service';
import { HttpClientModule } from '@angular/common/http';
import { ReviewsService } from './service/reviews.service';
import { SpaService } from './service/spa-services.service';  
 

const appRoutes: Routes = [
  { path: "", component: HomePageComponent },
  { path: "search-page", component: SearchPageComponent }  
];
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SearchPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule
  ],
  providers: [CategoryService,SpaService,ReviewsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
