import { TestBed } from '@angular/core/testing';

import { SpaServicesService } from './spa-services.service';

describe('SpaServicesService', () => {
  let service: SpaServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpaServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
